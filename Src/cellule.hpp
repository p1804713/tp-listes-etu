#ifndef LIFAP6_LISTE_CELLULE_HPP
#define LIFAP6_LISTE_CELLULE_HPP

class Cellule {

  public :
        int valeur;
        Cellule *suivant;
        
        Cellule(){
            suivant=nullptr;
        }
        
        Cellule(int val){
            valeur=val;
            suivant=nullptr;
        }

        
} ;

#endif
