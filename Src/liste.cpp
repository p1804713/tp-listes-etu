#include "liste.hpp"

#include <iostream>
#include <cassert>

#include <cstdio>

//Constructeur
Liste::Liste() {
  /* votre code ici */
    //nbElem= 0;
    first= nullptr;
}


//Destrcuteur
Liste::~Liste() {
    supprimer_en_tete();
  
}



Liste::Liste(const Liste& autre) {
  /* votre code ici */
  first= nullptr;
  Cellule * temp= autre.first;
  while(temp != nullptr)
  {
    ajouter_en_queue(temp->valeur);
    temp= temp->suivant;
  }
}

Liste& Liste::operator=(const Liste& autre) {
  /* votre code ici */
  while(first != nullptr){
    supprimer_en_tete();
  }
  Cellule * temp= autre.first;
  while( temp != nullptr){
    ajouter_en_queue(temp->valeur);
    temp= temp->suivant;
  }
  return *this;
}



void Liste::ajouter_en_tete(int v) {
    Cellule *newC = new Cellule(v);
    newC->suivant= first;
    first = newC;
  
}

void Liste::ajouter_en_queue(int valeur) {
  Cellule* temp= first;
  if(temp == nullptr){
    ajouter_en_tete(valeur);
  }
  else{
    while(temp->suivant != nullptr)
    {
      temp= temp ->suivant;
    }
    Cellule * newC = new Cellule(valeur);
    temp->suivant = newC;
  }
}

void Liste::supprimer_en_tete() {
  
}

Cellule* Liste::tete() {
  
  return this->first ;
}

const Cellule* Liste::tete() const {
  /* votre code ici */
  return nullptr ;
}

Cellule* Liste::queue() {
  if(first == nullptr){
    return nullptr;
  }
  else{
    Cellule* temp= this->first;
    while(temp->suivant != nullptr)
    {
      temp=temp->suivant;
    }
    return temp;
  }
}


const Cellule* Liste::queue() const {
  if(first == nullptr){
    return nullptr;
  }
  else{
    Cellule* temp= this->first;
    while(temp->suivant != nullptr)
    {
      temp=temp->suivant;
    }
    return temp;
  }
}

int Liste::taille() const {
  /* votre code ici */
  return 0 ;
}

Cellule* Liste::recherche(int valeur) {
  Cellule* temp= first;
  if(temp == nullptr){
    return nullptr;
  }
  else{
    while(temp->suivant != nullptr)
    {
      if (valeur == temp->valeur){
        return temp;
      } 
      temp= temp ->suivant;
    }
    return temp;
  }
}

const Cellule* Liste::recherche(int valeur) const {
  /* votre code ici */
  return nullptr ;
}

void Liste::afficher() const {
  Cellule* temp = first;
  
  printf("Affichage liste :\n");
    while (temp!= NULL)
    {
      std::printf("%d\n", temp->valeur);
        temp = temp->suivant;
      
    }
}
